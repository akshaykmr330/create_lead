package wdMethods;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

public class SeMethods implements WdMethods{
	public RemoteWebDriver driver;	
	public int i = 1;
	public void startApp(String browser, String url) {
		if(browser.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
			driver = new ChromeDriver();
		} else if(browser.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver", "./drivers/geckodrivers.exe");
			driver = new FirefoxDriver();
		}
		driver.manage().window().maximize();
		driver.get(url);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		System.out.println("The Browser "+browser+" Launched Successfully");
		takeSnap();
	}


	public WebElement locateElement(String locator, String locValue) {
		switch (locator) {
		case "id": return driver.findElementById(locValue);
		case "classname": return driver.findElementByClassName(locValue);
		case "xpath": return driver.findElementByXPath(locValue);
		case "linktext": return driver.findElementByLinkText(locValue);
		case  "name": return driver.findElementByName(locValue);
		}
		return null;
	}
	@Override
	public WebElement locateElement(String locValue) {

		return null;
	}

	@Override
	public void type(WebElement ele, String data) {
		ele.clear();
		ele.sendKeys(data);
		System.out.println("The Data "+data+" Entered Successfully");
		takeSnap();
	}

	@Override
	public void click(WebElement ele) {
		try {
			ele.click();
			System.out.println("The Element "+ele+" is clicked Successfully");
		} catch (ElementNotInteractableException e) {
			System.err.println("The Element "+ele+" is not clicked");
		}
		finally {
			takeSnap();
		}

	}

	public void clickWithNoSnap(WebElement ele) {
		ele.click();
		System.out.println("The Element "+ele+" is clicked Successfully");
	}

	@Override
	public String getText(WebElement ele) {
		String text = "";
		try {
			text = ele.getText();
			System.out.println("element text: "+text);

		} catch (Exception e) {
			System.err.println("no element text: ");
		}
		return text;

	}


	public void selectDropDownUsingText(WebElement ele, String value) {
		try {
			Select obj=new Select(ele);
			obj.selectByVisibleText(value);
		} catch (NoSuchElementException e) {
			System.err.println("There ele + no such elemet");
		}

		finally {
			takeSnap();
		}


	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {

		try {
			Select obj=new Select(ele);
			obj.selectByIndex(index);
		} catch (NoSuchElementException e) {
			System.err.println("There ele + no such elemet");
		}

		finally {
			takeSnap();
		}

	}

	@Override
	public boolean verifyTitle(String expectedTitle) {
		if (driver.getTitle().equals(expectedTitle)) {
			System.out.println("title matched");
			return true;
		} else {
			System.out.println("title not matched");
		}
		return false;
	}

	@Override
	public void verifyExactText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub
		if (ele.getText().equals(expectedText)) {
			System.out.println("text matched");
		} else {
			System.out.println("text not matched");
		}

	}

	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifySelected(WebElement ele) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyDisplayed(WebElement ele) {
		// TODO Auto-generated method stub

	}

	@Override
	public void switchToWindow(int index) {
		try {
			Set<String> set = driver.getWindowHandles();
			List<String> st=new ArrayList<>();
			st.addAll(set);
			driver.switchTo().window(st.get(index));
		} catch (NoSuchWindowException e) {
			System.err.println("There is no such window");
		}
		
	}

	@Override
	public void switchToFrame(WebElement ele) {
		try {
			driver.switchTo().frame(ele);
		} catch (NoSuchFrameException e) {
			System.err.println("Ther is no such frame");
		}
		finally {
		takeSnap();	
		}

	}

	@Override
	public void acceptAlert() {
		try {
			driver.switchTo().alert().accept();
		} catch (NoAlertPresentException e) {
			System.err.println("No alert presentexception");
		}
		

	}

	@Override
	public void dismissAlert() {
		// TODO Auto-generated method stub
		try {
			driver.switchTo().alert().dismiss();
		} catch (NoAlertPresentException e) {
			System.err.println("No alert presentexception");
		}
		
	}

	@Override
	public String getAlertText() {
		String text ="";
		try {
			text = driver.switchTo().alert().getText();
			
		} catch (NoAlertPresentException e) {
			System.err.println("No alert presentexception");
		}
		return text;
	}


	public void takeSnap() {
		try {
			File src = driver.getScreenshotAs(OutputType.FILE);
			File desc = new File("./snaps/img"+i+".png");
			FileUtils.copyFile(src, desc);
		} catch (IOException e) {
			e.printStackTrace();
		}
		i++;
	}

	@Override
	public void closeBrowser() {
		// TODO Auto-generated method stub
		driver.close();
	}

	@Override
	public void closeAllBrowsers() {
		// TODO Auto-generated method stub
		driver.quit();
	}

}

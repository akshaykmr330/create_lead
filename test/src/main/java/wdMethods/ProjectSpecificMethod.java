package wdMethods;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import excel.LearnExcel;

public class ProjectSpecificMethod extends SeMethods {
	public String excelFileName;
	
	
	@DataProvider(name ="fetchData")
	public Object[][] getData() throws IOException{
		return LearnExcel.excelData(excelFileName);
		}
	
	@Parameters({"browser","url","uname","pwd"})
	
	@BeforeMethod(groups="common")
	public void dologin() {
		startApp("chrome","http://leaftaps.com/opentaps");
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("id", "password");
		type(elePassword, "crmsfa");
		WebElement elelogin = locateElement("classname","decorativeSubmit");
		click(elelogin);
		WebElement elecrmsfa = locateElement("linktext","CRM/SFA");
		click(elecrmsfa);
		
	
		
	}
	@AfterMethod(groups="common")
	public void closeapp() {
		closeBrowser();
	}
}
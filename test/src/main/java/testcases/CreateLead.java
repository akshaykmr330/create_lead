package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;

import org.testng.annotations.Test;

import wdMethods.ProjectSpecificMethod;

public class CreateLead extends ProjectSpecificMethod{
	@BeforeClass(groups="common")
	public void setData() {
		excelFileName="CreateLead";
	}
	
    @Test(dataProvider="fetchData")
			//dataProviderClass=ProjectSpecificMethod.class)
	public void createLead(String compname, String firstname, 
            String lname){
		
	
	    WebElement elecrelead = locateElement("linktext","Create Lead");
		click(elecrelead);
		WebElement elecomname = locateElement("id","createLeadForm_companyName");
		type(elecomname,compname);
		WebElement elefirname = locateElement("id","createLeadForm_firstName");
		type(elefirname,firstname);
		WebElement elelastname = locateElement("id","createLeadForm_lastName");
		type(elelastname,lname);
		WebElement eleclickcrelead = locateElement("xpath", "//input[@name='submitButton']");
		click(eleclickcrelead);
		
		
	}
	/*@DataProvider(name="fetchData") 
	public static String[][] getData() {
		String[][] data = new String[2][3];
		data[0][0] = "comodo";
		data[0][1] = "hari";
		data[0][2] = "K";
		
		data[1][0] = "TCS";
		data[1][1] = "Ram";
		data[1][2] = "c"; 
		return data;*/
	
	
	
	
    }
	










